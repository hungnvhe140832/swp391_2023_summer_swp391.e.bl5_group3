/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.google.gson.Gson;
import Dao.DAOFeedback;
import Dao.DAOHouse;
import Model.Account;
import Model.Feedback;
import Model.House;
import Model.InforOwner;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author kienb
 */
public class Controller_Add_Feedback extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            Account acc = (Account) session.getAttribute("acc");
            String content = request.getParameter("content");
            int houseID = Integer.parseInt(request.getParameter("houseid"));
            DAOHouse dAOHouse = new DAOHouse();
            House h = dAOHouse.getHouseById(houseID);
            InforOwner inforOfOwner = dAOHouse.getInforOfOwner(houseID);
            h.setHouse_Owner_ID(inforOfOwner.getId());
            DAOFeedback dao = new DAOFeedback();
            dao.insertFeedBack(h, acc.getId(), content);
            List<Feedback> list = dao.getFeedbackByHouseId(houseID);
            Gson gson = new Gson();
            String json = gson.toJson(list);
            out.println(json);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
